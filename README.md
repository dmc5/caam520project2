# README #


Swift-Hohenberg Equation implementation for CAAM 520 Group Assignment

#####
 
Marielle Morris

David Clark

Peter Kulma

Cyna Shirazinejad 

Ethan Myers 

######

Currently run using the following options:
 
./swift-hohenberg -rho_petscspace_order 1 -rho_petscfe_default_quadrature_order 2 -dim 2 -pc_type lu -sn
es_type newtonls -ts_type beuler -mms 1 -dm_view -options_left -ts_dt 0.1 -dm_refine 2