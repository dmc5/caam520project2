static char help[] = "Swift-Hohenberg problem in 2D.\n\
We solve the Swift-Hohenberg equation \n\
https://en.wikipedia.org/wiki/Swift�Hohenberg_equation\n\n\n";

#include <petscdmplex.h>
#include <petscsnes.h>
#include <petscts.h>
#include <petscds.h>

/*F
  Swift-Hohenberg equation, (modified for 2 component field):

\begin{align*}
       u_t &= (r-1)u + gu^2 - u^3 + 2f+ \delta f
       f = \delta u
\end{align*}
F*/

typedef struct {
  PetscInt          dim;
  PetscBool         simplex;
  PetscInt          mms;
  PetscErrorCode (**exactFuncs)(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void *ctx);
} AppCtx;


/*
  we dont use mms 0
*/

PetscErrorCode mms0(PetscInt dim, PetscReal t, const PetscReal x[], PetscInt Nf, PetscScalar *u, void *ctx)
{
  u[0] = PetscSinReal(x[0]*20.0);
  u[1] = PetscCosReal(x[1]*20.0);
  return 0;
}

/* MMS1

  u = t + \cos{\pi x} \cos{\pi y};
  f = -2 \pi^2 \cos{\pi x} \cos{pi y}


*/
PetscErrorCode mms1(PetscInt dim, PetscReal t, const PetscReal x[], PetscInt Nf, PetscScalar *u, void *ctx)
{
  u[0] = PetscCosReal(x[0]*PETSC_PI)*PetscCosReal(x[1]*PETSC_PI) + t;
  u[1] = PetscCosReal(x[0]*PETSC_PI)*PetscCosReal(x[1]*PETSC_PI) * (-2.0*PetscPowReal(PETSC_PI, 2));
  return 0;
}

static void f0(PetscInt dim, PetscInt Nf, PetscInt NfAux,
               const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
               const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
               PetscReal t, const PetscReal x[], PetscScalar f0[])
{
  PetscReal      r=0.0;
  PetscReal      g=1.0;

  /* Reaction */
  f0[0] = (r-1)*u[0]+g*PetscPowReal(u[0], 2)-PetscPowReal(u[0], 3)+2*u[1]-u_t[0];
  f0[1] = -u[1];
}

static void f0_mms1(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                    const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                    const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                    PetscReal t, const PetscReal x[], PetscScalar f0[])
{
  PetscReal      uexact   = PetscCosReal(x[0]*PETSC_PI)*PetscCosReal(x[1]*PETSC_PI) + t;
  PetscReal      fexact   = PetscCosReal(x[0]*PETSC_PI)*PetscCosReal(x[1]*PETSC_PI) * (-2.0*PetscPowReal(PETSC_PI, 2.0));
  PetscReal      flaplace = PetscCosReal(x[0]*PETSC_PI)*PetscCosReal(x[1]*PETSC_PI) * (4.0*PetscPowReal(PETSC_PI, 4.0));
  PetscReal      r=1.0;
  PetscReal      g=0.0;

  f0[0] = u_t[0] + 2.0 * u[1] + PetscPowReal(u[0], 3.0) - g * PetscPowReal(u[0], 2.0) - (r-1.0)*u[0];
  f0[1] = u[1];
  
  /* MMS Forcing */
  f0[0] -= (1.0 + 2.0 * fexact + flaplace + PetscPowReal(uexact, 3.0) - g * PetscPowReal(uexact, 2.0) - (r-1.0)*uexact);
  f0[1] -= 0.0;
}

static void f1(PetscInt dim, PetscInt Nf, PetscInt NfAux,
               const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
               const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
               PetscReal t, const PetscReal x[], PetscScalar f1[])
{
  const PetscInt Nc   = dim;
  PetscInt       c, d;

  for (c = 0; c < Nc; ++c) {
    for (d = 0; d < dim; ++d) {
      switch (c) {
        case 0:
          f1[c*dim+d] = -u_x[1*dim+d]; break;
        case 1:
          f1[c*dim+d] = u_x[0*dim+d]; break;
      }
    }
  }
}


static void g0(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                  const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                  const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                  PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscScalar g0[])
{
  const PetscInt Nc     = dim;
  PetscReal      r=1.0;
  PetscReal      g=0.0;
  PetscReal      c=0.0;

 
  g0[0*Nc+0] += u_tShift;
  
  /* dF/du */
  g0[0*Nc+0] += -(r-1) - 2.0 * g * u[0] + 3.0 * PetscPowReal(u[0], 2.0);
  /* dF/dv */
  g0[0*Nc+1] += 2.0;
  /* dG/du */
  g0[1*Nc+0] += 0.0;
  /* dG/dv */
  g0[1*Nc+1] += 1.0;
}

/* < \nabla v, \nabla u + {\nabla u}^T > */
static void g3(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                  const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                  const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                  PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscScalar g3[])
{
  const PetscInt Nc   = dim;
  PetscInt fc,gc,df,dg;

  for (fc=0; fc < Nc;++fc){
    for(gc=0; gc < Nc; ++gc){
      for (df = 0; df <dim; ++df){
        for(dg = 0;dg<dim; ++dg){
          if ((fc != gc) && (df == dg)){
            g3[((fc*Nc + gc)*dim + df)*dim +dg] = fc == 0 ? -1.0 : 1.0;
          }
        }
      }
    }  
  }
}

static PetscErrorCode ProcessOptions(MPI_Comm comm, AppCtx *options)
{
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  options->dim     = 2;
  options->simplex = PETSC_TRUE;
  options->mms     = 1;

  ierr = PetscOptionsBegin(comm, "", "Fitzhugh-Nagumo Equation Options", "DMPLEX");CHKERRQ(ierr);
  ierr = PetscOptionsInt("-dim", "The topological mesh dimension", "turing.c", options->dim, &options->dim, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-simplex", "Simplicial (true) or tensor (false) mesh", "turing.c", options->simplex, &options->simplex, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-mms", "The manufactured solution to use", "turning.c", options->mms, &options->mms, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();
  PetscFunctionReturn(0);
}

static PetscErrorCode CreateBCLabel(DM dm, const char name[])
{
  DMLabel        label;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMCreateLabel(dm, name);CHKERRQ(ierr);
  ierr = DMGetLabel(dm, name, &label);CHKERRQ(ierr);
  ierr = DMPlexMarkBoundaryFaces(dm, label);CHKERRQ(ierr);
  ierr = DMPlexLabelComplete(dm, label);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode CreateMesh(MPI_Comm comm, DM *dm, AppCtx *ctx)
{
  DM             pdm = NULL;
  const PetscInt dim = ctx->dim;
  PetscInt       cells[3] = {1, 1, 1}; /* coarse mesh is one cell; refine from there */
  PetscBool      hasLabel;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  if (ctx->simplex) {
    ierr = DMPlexCreateBoxMesh(comm, dim, dim == 2 ? 2 : 1, PETSC_TRUE, dm);CHKERRQ(ierr);
  } else {
    ierr = DMPlexCreateHexBoxMesh(comm, dim, cells, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, dm);CHKERRQ(ierr);
  }
  ierr = PetscObjectSetName((PetscObject) *dm, "Mesh");CHKERRQ(ierr);
  /* If no boundary marker exists, mark the whole boundary */
  ierr = DMHasLabel(*dm, "marker", &hasLabel);CHKERRQ(ierr);
  if (!hasLabel) {ierr = CreateBCLabel(*dm, "marker");CHKERRQ(ierr);}
  /* Distribute mesh over processes */
  ierr = DMPlexDistribute(*dm, 0, NULL, &pdm);CHKERRQ(ierr);
  if (pdm) {
    ierr = DMDestroy(dm);CHKERRQ(ierr);
    *dm  = pdm;
  }
  ierr = DMSetFromOptions(*dm);CHKERRQ(ierr);
  ierr = DMViewFromOptions(*dm, NULL, "-dm_view");CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode SetupProblem(PetscDS prob, AppCtx *ctx)
{
  const PetscInt id = 1;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  switch (ctx->mms) {
  case 0:
    ierr = PetscDSSetResidual(prob, 0, f0, f1);CHKERRQ(ierr);break;
  case 1:
    ierr = PetscDSSetResidual(prob, 0, f0_mms1, f1);CHKERRQ(ierr);break;
  default:
    SETERRQ1(PetscObjectComm((PetscObject) prob), PETSC_ERR_ARG_OUTOFRANGE, "Unknown MMS %D", ctx->mms);
  }
  ierr = PetscDSSetJacobian(prob, 0, 0, g0, NULL, NULL, g3);CHKERRQ(ierr);
  switch (ctx->dim) {
  case 2:
    switch (ctx->mms) {
    case 0:
      ctx->exactFuncs[0] = mms0;
      break;
    case 1:
      ctx->exactFuncs[0] = mms1;
      break;
    default:
      SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_ARG_OUTOFRANGE, "Unknown MMS %D", ctx->mms);
    }
    break;
  default:
    SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_ARG_OUTOFRANGE, "Invalid dimension %D", ctx->dim);
  }
  if (ctx->mms) {
    ierr = PetscDSAddBoundary(prob, DM_BC_ESSENTIAL, "wall", "marker", 0, 0, NULL, (void (*)()) ctx->exactFuncs[0], 1, &id, ctx);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

static PetscErrorCode SetupDiscretization(DM dm, AppCtx *ctx)
{
  DM              cdm = dm;
  const PetscInt  dim = ctx->dim;
  PetscDS         prob;
  PetscFE         fe;
  PetscErrorCode  ierr;

  PetscFunctionBeginUser;
  /* Create finite element */
  ierr = PetscFECreateDefault(dm, dim, dim, ctx->simplex, "rho_", PETSC_DEFAULT, &fe);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) fe, "concentration");CHKERRQ(ierr);
  /* Set discretization and boundary conditions for each mesh */
  ierr = DMGetDS(dm, &prob);CHKERRQ(ierr);
  ierr = PetscDSSetDiscretization(prob, 0, (PetscObject) fe);CHKERRQ(ierr);
  ierr = SetupProblem(prob, ctx);CHKERRQ(ierr);
  while (cdm) {
    PetscBool hasLabel;

    ierr = DMSetDS(cdm, prob);CHKERRQ(ierr);
    ierr = DMHasLabel(cdm, "marker", &hasLabel);CHKERRQ(ierr);
    if (!hasLabel) {ierr = CreateBCLabel(cdm, "marker");CHKERRQ(ierr);}
    ierr = DMGetCoarseDM(cdm, &cdm);CHKERRQ(ierr);
  }
  ierr = PetscFEDestroy(&fe);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode MonitorError(TS ts, PetscInt step, PetscReal crtime, Vec u, void *ctx)
{
  AppCtx        *user = (AppCtx *) ctx;
  DM             dm;
  PetscReal      ferrors[1] = {0.0};
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = TSGetDM(ts, &dm);CHKERRQ(ierr);
  if (user->mms) {
    ierr = DMComputeL2FieldDiff(dm, crtime, user->exactFuncs, NULL, u, ferrors);CHKERRQ(ierr);
  }
  ierr = PetscPrintf(PETSC_COMM_WORLD, "Timestep: %04d time = %-8.4g \t L_2 Error: [%2.3g]\n", (int) step, (double) crtime, (double) ferrors[0]);CHKERRQ(ierr);
  ierr = VecViewFromOptions(u, NULL, "-sol_vec_view");CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

int main(int argc, char **argv)
{
  AppCtx         ctx;
  DM             dm;
  TS             ts;
  Vec            u, r;
  PetscErrorCode ierr;

  ierr = PetscInitialize(&argc, &argv, NULL, help);CHKERRQ(ierr);
  ierr = ProcessOptions(PETSC_COMM_WORLD, &ctx);CHKERRQ(ierr);
  ierr = CreateMesh(PETSC_COMM_WORLD, &dm, &ctx);CHKERRQ(ierr);
  ierr = DMSetApplicationContext(dm, &ctx);CHKERRQ(ierr);
  ierr = PetscMalloc1(1, &ctx.exactFuncs);CHKERRQ(ierr);
  ierr = SetupDiscretization(dm, &ctx);CHKERRQ(ierr);
  ierr = DMPlexCreateClosureIndex(dm, NULL);CHKERRQ(ierr);

  ierr = DMCreateGlobalVector(dm, &u);CHKERRQ(ierr);
  ierr = VecDuplicate(u, &r);CHKERRQ(ierr);

  ierr = TSCreate(PETSC_COMM_WORLD, &ts);CHKERRQ(ierr);
  ierr = TSMonitorSet(ts, MonitorError, &ctx, NULL);CHKERRQ(ierr);
  ierr = TSSetDM(ts, dm);CHKERRQ(ierr);
  ierr = DMTSSetBoundaryLocal(dm, DMPlexTSComputeBoundary, &ctx);CHKERRQ(ierr);
  ierr = DMTSSetIFunctionLocal(dm, DMPlexTSComputeIFunctionFEM, &ctx);CHKERRQ(ierr);
  ierr = DMTSSetIJacobianLocal(dm, DMPlexTSComputeIJacobianFEM, &ctx);CHKERRQ(ierr);
  ierr = TSSetExactFinalTime(ts, TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
  ierr = TSSetFromOptions(ts);CHKERRQ(ierr);

  ierr = DMProjectFunction(dm, 0.0, ctx.exactFuncs, NULL, INSERT_ALL_VALUES, u);CHKERRQ(ierr);
  ierr = TSSolve(ts, u);CHKERRQ(ierr);
  ierr = VecViewFromOptions(u, NULL, "-sol_final_vec_view");CHKERRQ(ierr);

  ierr = VecDestroy(&u);CHKERRQ(ierr);
  ierr = VecDestroy(&r);CHKERRQ(ierr);
  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  ierr = PetscFree(ctx.exactFuncs);CHKERRQ(ierr);
  ierr = PetscFinalize();
  return ierr;
}
